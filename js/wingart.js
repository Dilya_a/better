$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ



// INCLUDE FUNCTION

var starRating 		= $(".example-css"),
	stripeRating 	= $(".example-1to10"),
	equalheight 	= $(".equalheight");

if(starRating.length || stripeRating.length){
  include("js/jquery.barrating.min.js");
}

if(equalheight.length){
  include("js/jquery.equalheights.js");
}


function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}




$(document).ready(function(){

  // ДЕЛАЕМ ИНИТ ФУНКЦИЙ ЗДЕСЬ

  // ---------  Start Rating  ---------
  	if(starRating.length){
      $(starRating).each(function() {
        var currentInitVal = parseFloat($(this).data('current-rating'));

        $(this).barrating({
              theme: 'css-stars',
              showSelectedRating: false,
              readonly: true,
              initialRating: currentInitVal
          });
      })
  	}

  	if(stripeRating.length){
  		$(stripeRating).barrating('show', {
            theme: 'bars-1to10',
            readonly: true
        });
  	}
  //------------  EQUALHEIGHT  ------------
  	if(equalheight.length){
  		$(equalheight).equalHeights();
  	}


})